package com.vertx.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.ext.web.RequestBody;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.util.logging.Logger;

public class ApiVerticle extends AbstractVerticle {
    private static Logger log =
            Logger.getLogger(ApiVerticle.class.getPackageName());

    @Override
    public void start(Promise<Void> startPromise) {
        Router router = createRouter();
        vertx.createHttpServer()
                .requestHandler(router)
                .listen(8080, http -> {
                    if (http.succeeded()) {
                        startPromise.complete();
                        System.out.println
                                ("HTTP server started on port 8080");
                    } else {
                        startPromise.fail(http.cause());
                    }
                });
    }

    private Router createRouter() {
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.get("/").handler(this::helloHandler);
        router.post("/student/create").handler(this::createStudentHandler);
        router.get("/student").handler(this::listingStudentHandler);
        return router;
    }

    private void helloHandler(RoutingContext context) {
        log.info("ACCESS 200 [/]");
        context.response()
                .putHeader("content-type", "text/plain")
                .end("Hello from Vert.x!");
    }

    private void createStudentHandler(RoutingContext context) {
        RequestBody body = context.body();
        vertx.eventBus().request("student.create", body.asJsonObject(), handler -> {
            if (handler.succeeded()) {
                log.info("ACCESS 200 [/student/create]");
                context.response()
                        .putHeader("content-type", "application/json")
                        .end(handler.result().body().toString());
            } else {
                log.info("ACCESS 500 [/student/create]");
                context.response().setStatusCode(500)
                        .end(handler.cause().getMessage());
            }
        });
    }

    private void listingStudentHandler(RoutingContext context) {
        vertx.eventBus().request("student.listing", "", handler -> {
            if (handler.succeeded()) {
                log.info("ACCESS 200 [/student/create]");
                context.response()
                        .putHeader("content-type", "application/json")
                        .end(handler.result().body().toString());
            } else {
                log.info("ACCESS 500 [/student/create]");
                context.response().setStatusCode(500)
                        .end(handler.cause().getMessage());
            }
        });
    }
}