package com.vertx.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.vertx.sqlclient.templates.annotations.RowMapped;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class Student implements Serializable {
    @JsonProperty(value = "id")
    private Integer id;
    @JsonProperty(value = "name")
    private String name;
    @JsonProperty(value = "age")
    private Integer age;
}
