package com.vertx.verticle;

import com.vertx.data.response.ResponseEntity;
import com.vertx.model.Student;
import com.vertx.repository.StudentRepository;
import com.vertx.service.StudentService;
import com.vertx.utils.DbUtils;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgPool;

import java.util.logging.Logger;

public class StudentVerticle extends AbstractVerticle {
    private static Logger log =
            Logger.getLogger(StudentVerticle.class.getPackageName());

    @Override
    public void start(Promise<Void> promise) {
        final PgPool dbClient = DbUtils.buildDbClient(vertx);

        final StudentRepository studentRepository = new StudentRepository();
        final StudentService studentService = new StudentService(dbClient, studentRepository);


        vertx.eventBus().consumer("student.create", m -> {
            String dataString = m.body().toString();
            JsonObject dataObject = new JsonObject(dataString);
            Student student;
            student = new Student().setName(dataObject.getString("name")).setAge(dataObject.getInteger("age"));
            studentService.create(student)
                    .onSuccess(result -> m.reply(Json.encode(ResponseEntity.okEntity(result))))
                    .onFailure(failure -> m.reply(Json.encode(ResponseEntity.okEntity(failure.getMessage())))
            );
        });

        vertx.eventBus().consumer("student.listing", m -> {
            studentService.listingAll()
                    .onSuccess(result -> m.reply(Json.encode(ResponseEntity.okEntity(result))))
                    .onFailure(failure -> m.reply(Json.encode(ResponseEntity.okEntity(failure.getMessage())))
                    );
        });

        promise.complete();
    }
}