package com.vertx.service;

import com.vertx.model.Student;
import com.vertx.repository.StudentRepository;
import io.vertx.core.Future;
import io.vertx.pgclient.PgPool;

import java.util.List;


public class StudentService {
    private final PgPool dbClient;
    private final StudentRepository studentRepository;

    public StudentService(PgPool dbClient, StudentRepository studentRepository) {
        this.dbClient = dbClient;
        this.studentRepository = studentRepository;
    }

    public Future<Student> create(Student student) {
        return dbClient.withConnection(
                        connection -> {
                            return studentRepository.insert(connection, student);
                        })
                .onSuccess(success -> System.out.println("create student success"))
                .onFailure(failure -> System.out.println("create student failure"));
    }

    public Future<List<Student>> listingAll() {
        return dbClient.withConnection(
                        connection -> {
                            return studentRepository.selectAll(connection);
                        })
                .onSuccess(success -> System.out.println("create student success"))
                .onFailure(failure -> System.out.println("create student failure"));
    }
}
