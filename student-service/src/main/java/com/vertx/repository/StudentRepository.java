package com.vertx.repository;

import com.vertx.model.Student;
import io.vertx.core.Future;
import io.vertx.sqlclient.RowIterator;
import io.vertx.sqlclient.SqlConnection;
import io.vertx.sqlclient.templates.SqlTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class StudentRepository {
    private static final String SQL_INSERT = "INSERT INTO students (name, age) VALUES (#{name}, #{age}) RETURNING id";
    private static final String SQL_SELECT_ALL = "SELECT * FROM students";

    public Future<Student> insert(SqlConnection connection, Student student) {
        return SqlTemplate.forUpdate(connection, SQL_INSERT)
                .mapFrom(Student.class)
                .mapTo(Student.class)
                .execute(student)
                .map(rowSet -> {
                            final RowIterator<Student> iterator = rowSet.iterator();

                            if (iterator.hasNext()) {
                                student.setId(iterator.next().getId());
                                return student;
                            } else throw new NoSuchElementException("No student");
                        }
                )
                .onSuccess(success -> System.out.println("Insert book Success"))
                .onFailure(failure -> System.out.println(failure.getMessage()));

    }

    public Future<List<Student>> selectAll(SqlConnection connection) {
        return SqlTemplate.forQuery(connection, SQL_SELECT_ALL)
                .mapTo(Student.class)
                .execute(null)
                .map(rowSet -> {
                    final List<Student> students = new ArrayList<>();
                    rowSet.forEach(students::add);
                    return students;
                })
                .onSuccess(success -> System.out.println("Listing All Success"))
                .onFailure(failure -> System.out.println(failure.getMessage()));

    }
}
