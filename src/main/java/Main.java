import io.vertx.core.Vertx;

public class Main {
    public static void main(String[] args) {
        Vertx.vertx().deployVerticle(
                new ApiVerticle(),deployHandler->{
                    if(deployHandler.succeeded()){
                        System.out.println
                                ("REST Verticle Deployed");
                    } else {
                        System.out.println
                                (deployHandler.cause().getMessage());
                    }
                });
    }
}
